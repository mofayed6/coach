<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function index()
    {

    }

    public function create(Request $request)
    {
        return view('backend.users.create');
    }
}
