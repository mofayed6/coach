<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('backend.auth.login');
    }

    public function doLogin(Request $request)
    {


        $request->validate([

            'password' => 'required|min:6',

            'email' => 'required|email'

        ], [

            'email.required' => 'Email is required',

            'password.required' => 'Password is required'

        ]);



        if(auth()->attempt($request->only(['email','password']),$request->rememberme)){
            $cartSession = session('cart');
            if($cartSession  ){
                return redirect(route('user_order.checkout', app()->getLocale()));
            }else{

                return redirect()->intended('/');

            }
        }

        return redirect('/login')
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => 'Incorrect email address or password',
            ]);
    }

    public function logout()
    {
        if (auth()->check()) {
            \Auth::logout();
        }
        return redirect(url('/'));
    }

    public function showLinkRequestForm()
    {
        return view('front.auth.passwords.email');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);
        $userModel = User::where('email', $request->only('email','activation_code'))->first();
        Mail::to($userModel->email)->send(new passwordNotification($userModel));
        return redirect(url('/'))->with('success','check your email');
    }

    public function resendRememberToken()
    {
        return view('front.auth.passwords.reset');
    }

}
